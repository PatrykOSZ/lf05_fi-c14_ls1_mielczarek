// 3.2 AB Methoden I -> Aufgabe 5

import java.util.Scanner;

public class Aufgabe5 {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		int indexLand = 0;
		double budget = 0.0, zuUmtauschen = 0.0, budgetInFremdwaehrung = 0.0;
		boolean reiseLaeuft = true;
		String land[] = { "USA", "Japan", "Gro�brittanien", "Schweiz", "Schweden" };
		String waehrung[] = { "USD", "JPN", "GBP", "CHF", "SEK" };
		String gastland;
		
		begruesung();
		verfuegbareLaender(land);
		budget = startBudget();
		
		while(reiseLaeuft) {
			System.out.printf("Verf�gbarer Budget: %.2f EUR.\n", budget);
			do {
				gastland = eingabeLand();
				indexLand = gastlandPruefen(land, gastland);
				if(indexLand == -1)
					System.out.printf("Falsche Eingabe!\n");
			}while(indexLand == -1);

			do {
				zuUmtauschen = eingabeUmtauschmenge();	
				if(zuUmtauschen > budget)
					System.out.printf("Sie haben nicht genug Geld um die Summe von %.2f umtauschen zu k�nnen!\n", zuUmtauschen);
			}while(zuUmtauschen > budget);
			
			budgetInFremdwaehrung = umrechnung(indexLand, zuUmtauschen);
			
			if(budgetInFremdwaehrung == -1) {
				System.out.println("Bei der Umrechnung des Betrages ist irgendwas schiefgelaufen!");
				System.exit(0);
			}
			
			System.out.printf("Sie haben %.2f EUR zu %.2f %s umgetauscht.\nViel Spa� in %s.\n", zuUmtauschen, budgetInFremdwaehrung, waehrung[indexLand], land[indexLand]);
			
			warten(500);
			
			budget = aktuellerBudget(budget, zuUmtauschen);
			reiseLaeuft = pruefungBudged(budget);
			
			if(!pruefungBudged(budget)) {
				System.out.print("\nSie haben weniger als 100,00 EUR zur Verf�gung.\n");
				abschiedstext();
				System.exit(0);
			}
			
			reiseLaeuft = pruefungEndeReise();
			if(reiseLaeuft) {
				abschiedstext();
			}
		}

		scan.close();
	}

	public static void begruesung() {
		System.out.println("******* Willkommen auf Ihrer Reise *******");
	}
	
	public static double startBudget() {
		System.out.print("Geben Sie bitte Ihren gesamten Budget f�r die Reise in Euro ein: ");
		return scan.nextDouble();
	}
	
	public static void verfuegbareLaender(String[] land) {
		System.out.print("Sie wollen die L�nder ");
		int anzahlLaender = land.length;
		for(int i = 0; i < anzahlLaender; i++) {
			System.out.printf("%s", land[i]);
			if(i < (anzahlLaender - 2))
				System.out.printf("%s", ", ");
			else if(i < (anzahlLaender -1))
				System.out.printf("%s", " und ");
			else
				continue;
		}
		
		System.out.print(" besuchen.\n\n");
	}
	
	public static String eingabeLand() {
		System.out.print("In welchen Gastland sind sie? ");
		return scan.next().toLowerCase();
	}
	
	private static double aktuellerBudget(double budget, double zuUmtauschen) {
		return budget -= zuUmtauschen;
	}
	
	private static int gastlandPruefen(String[] land, String gastland) {
		for(int i = 0; i < land.length; i++) {
			if(gastland.equals(land[i].toLowerCase())) {
				return i;
			}
		}
		
		return -1;
	}

	private static double eingabeUmtauschmenge() {
		System.out.print("Wie viel EUR m�chten Sie umtauschen? ");
		return scan.nextDouble();
	}

	private static double umrechnung(int indexLand, double zuUmtauschen) {
		if(indexLand == 0)
			return zuUmtauschen * 1.22;
		else if(indexLand == 1)
			return zuUmtauschen * 126.50;
		else if(indexLand == 2)
			return zuUmtauschen *  0.89;
		else if(indexLand == 3)
			return zuUmtauschen *  1.08;
		else if(indexLand == 4)
			return zuUmtauschen *  10.10;
		
		return -1;
	}

	private static boolean pruefungBudged(double budget) {
		if(budget <= 100) {
			return false;
		}
		return true;
	}

	private static void warten(int zeit) {
		for (int i = 0; i < 8; i++) {
			System.out.print("* ");
			try {
				Thread.sleep(zeit);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static boolean pruefungEndeReise() {
		if(eingabeEndeReise() == 'j') {
			return true;
		}
		return false;
	}

	private static char eingabeEndeReise() {
		char antwort;
		do {
			System.out.print("\nM�chten Sie nach Hause fahren (j/n)? ");
			antwort = scan.next().charAt(0);
		}while(antwort != 'j' && antwort != 'n');
		
		return antwort;
	}
	
	private static void abschiedstext() {
		System.out.print("Sie sind nun zu Hause.\nEs war eine sch�ne Reise.\nBis Bald.");
	}
}