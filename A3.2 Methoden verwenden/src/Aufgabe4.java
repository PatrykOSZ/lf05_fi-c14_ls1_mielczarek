// 3.2 AB Methoden I -> Aufgabe 4

import java.util.Scanner;

public class Aufgabe4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int auswahl;
		double a, b, c, h, r;
		
		System.out.print("F�r welche Form soll das Volumen berechnet werden?\n"
				+ "1 f�r W�rfel\n"
				+ "2 f�r Quader\n"
				+ "3 f�r Pyramide\n"
				+ "4 f�r Kugel\n");
		System.out.print("Geben Sie die Zahl Ihrer Wahl ein: ");
		auswahl = scan.nextInt();
		
		if(auswahl == 1) {
			System.out.print("\nGeben Sie die L�nge der Seite \"a\" ein: ");
			a = scan.nextDouble();
			
			System.out.printf("Das Volumen des W�rfels ist %.2f.\n", wuerfel(a));
		}
		else if(auswahl == 2) {
			System.out.print("\nGeben Sie die L�nge der Seite \"a\" ein: ");
			a = scan.nextDouble();
			System.out.print("\nGeben Sie die L�nge der Seite \"b\" ein: ");
			b = scan.nextDouble();
			System.out.print("\nGeben Sie die L�nge der Seite \"c\" ein: ");
			c = scan.nextDouble();
			
			System.out.printf("Das Volumen des Quaders ist %.2f.\n", quader(a, b, c));
		}
		if(auswahl == 3) {
			System.out.print("\nGeben Sie die L�nge der Seite \"a\" ein: ");
			a = scan.nextDouble();
			System.out.print("\nGeben Sie die H�he des K�rpers \"h\" ein: ");
			h = scan.nextDouble();
			System.out.printf("Das Volumen der Pyramide ist %.2f.\n", pyramide(a, h));
		}
		if(auswahl == 1) {
			System.out.print("\nGeben Sie die L�nge des Radius \"r\" ein: ");
			r = scan.nextDouble();
			System.out.printf("Das Volumen der Kugel ist %.2f.\n", kugel(r));
		}
		else {
			System.out.println("\nFalsche Eingabe!!!");
		}
		
		scan.close();
	}
	
	public static double wuerfel(double a) {
		return Math.pow(a, 3);
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double pyramide(double a, double h) {
		return Math.pow(a, 2) * h / 3;
	}
	
	public static double kugel(double r) {
		return 4 / 3 * Math.pow(r, 3) * Math.PI;
	}
}