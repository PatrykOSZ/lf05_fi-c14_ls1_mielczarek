// 3.2 AB Methoden I-> Aufgabe 2

import java.util.Scanner;

public class Aufgabe2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double zahl1, zahl2;
		
		System.out.print("Geben Sie die erste Zahl ein: ");
		zahl1 = scan.nextDouble();
		System.out.print("Geben Sie die zweite Zahl ein: ");
		zahl2 = scan.nextDouble();
		
		System.out.printf("Das Ergebnis der Multiplikation ist %.2f", multiplikation(zahl1, zahl2));
		
		scan.close();
	}
	
	public static double multiplikation(double zahl1, double zahl2) {
		return zahl1 * zahl2;
	}
}