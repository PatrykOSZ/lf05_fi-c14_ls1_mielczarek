// A1.5 Ausgabenformatierung 1 -> Aufgabe 3

public class Aufgabe3 {
	public static void main(String[] args) {
		// Formatierte Ausgabe der Konstanten
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
	}
}