// A1.5 Ausgabenformatierung 1 -> Aufgabe 2

public class Aufgabe2 {
	public static void main(String[] args) {
		// Ausgabe eines Pfeils
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***");
		System.out.println("     ***");
	}
}