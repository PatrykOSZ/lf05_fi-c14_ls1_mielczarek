// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 1

import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args) {
		// ERSTER TEIL DER AUFGABE
		Scanner scan = new Scanner(System.in);
		int num1, num2;
		
		System.out.print("Geben Sie eine Zahl ein: ");
		num1 = scan.nextInt();
		System.out.print("Geben Sie eine weitere Zahl ein: ");
		num2 = scan.nextInt();
		
		if(num1 == num2)
			System.out.println("Beide Zahlen sind gleich!");
		else if(num1 < num2)
			System.out.println("Zweite Zahl ist gr��er als die erste Zahl!");
		else
			System.out.println("Erste Zahl ist gr��er als die zweite Zahl!");
		
		// ZWEITER TEIL DER AUFGABE
		int num3;
		
		System.out.print("Geben Sie eine dritte Zahl ein: ");
		num3 = scan.nextInt();
		
		// 1.
		if(num1 > num2 && num1 > num3)
			System.out.println("Erste Zahl ist am gr��ten!");
		
		// 2.
		if(num3 > num1 || num3 > num2)
			System.out.println("Dritte Zahl ist gr��er als die erste Zahl oder als die zweite Zahl!");
		
		// 3.
		if(num1 > num2 && num1 > num3)
			System.out.printf("Die Zahl %d ist am gr��ten.\n", num1);
		else if(num2 > num1 && num2 > num3)
			System.out.printf("Die Zahl %d ist am gr��ten.\n", num2);
		else
			System.out.printf("Die Zahl %d ist am gr��ten.\n", num3);
		
		scan.close();
	}
}