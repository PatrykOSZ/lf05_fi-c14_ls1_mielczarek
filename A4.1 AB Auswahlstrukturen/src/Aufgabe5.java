// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 5

import java.util.Scanner;

public class Aufgabe5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char geschlecht;
		double gr��e, gewicht, bmi;
		
		System.out.print("Sind sie m�nnlich (m) oder weiblich (w)? ");
		geschlecht = scan.next().charAt(0);
		System.out.print("Geben Sie bitte ihre Gr��e in Meter ein: ");
		gr��e = scan.nextDouble();
		System.out.print("Geben Sie ihr gewicht in Kilogramm ein: ");
		gewicht = scan.nextDouble();
		
		bmi = gewicht / Math.pow(gr��e, 2);
		
		if(geschlecht == 'm') {
			if(bmi < 20)
				System.out.printf("Ihr BMI ist %.1f und Sie haben Untergewicht.\n", bmi);
			else if(bmi >= 20 && bmi <= 25)
				System.out.printf("Ihr BMI ist %.1f und Sie haben Normalgewicht.\n", bmi);
			else
				System.out.printf("Ihr BMI ist %.1f und Sie haben �bergewicht.\n", bmi);
		}
		else {
			if(bmi < 19)
				System.out.printf("Ihr BMI ist %.1f und Sie haben Untergewicht.\n", bmi);
			else if(bmi >= 19 && bmi <= 24)
				System.out.printf("Ihr BMI ist %.1f und Sie haben Normalgewicht.\n", bmi);
			else
				System.out.printf("Ihr BMI ist %.1f und Sie haben �bergewicht.\n", bmi);
		}
		
		scan.close();
	}
}