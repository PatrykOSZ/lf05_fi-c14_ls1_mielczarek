// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 3

import java.util.Scanner;

public class Aufgabe3 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int bestellmenge;
		double preisProArtikel = 9.99;
		double mwst = 0.19;
		double lieferkosten = 10.0;
		double rechnungsbetrag = 0.0;
		
		System.out.print("Anzahl der gew�nschten PC-M�use: ");
		bestellmenge = scan.nextInt();
		
		if(bestellmenge < 10)
			rechnungsbetrag += lieferkosten;
		
		rechnungsbetrag += (preisProArtikel * bestellmenge) * (1 + mwst);
		
		System.out.printf("Der Rechnungsbetrag ist %.2f EUR.", rechnungsbetrag);
		
		scan.close();
	}
}