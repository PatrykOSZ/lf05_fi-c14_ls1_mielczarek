// 4.1 Einführung Auswahlstrukturen -> Aufgabe 6

import java.util.Scanner;

public class Aufgabe6 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double e = 2.718, x = 0.0, ergebnis = 0.0;
		String bereich = "";
		
		
		System.out.print("Geben Sie ein Wert ein: ");
		x = scan.nextDouble();
		
		if(x <= 0) {
			ergebnis = fExponentiell(e, x);
			bereich = "exponentiell";
		}
		else if(x > 0 && x <= 3) {
			ergebnis = fQuadratisch(e, x);
			bereich = "quadratisch";
		}
		else if(x > 3) {
			ergebnis = fLinear(x);
			bereich = "linear";
		}
		else
			System.out.println("Eingabe ist keine Zahl !");
		
		System.out.printf("Der Bereich ist \"%s\" und das Ergebnis ist: %.2f.\n", bereich, ergebnis);
		
		scan.close();
	}
	
	public static double fExponentiell(double e, double x) {
		return Math.pow(e, x);
	}
	
	public static double fQuadratisch(double e, double x) {
		return (Math.pow(x, 2) + 1);
	}
	
	public static double fLinear(double x) {
		return 2 * x + 4;
	}
}