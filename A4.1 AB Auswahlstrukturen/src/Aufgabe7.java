// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 7

import java.util.Scanner;

public class Aufgabe7 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String eingabe;
		
		System.out.print("Geben Sie bitte drei Buchstaben/Zahlen in beliebiger Reihenfolge: ");
		eingabe = scan.next();
		
		// Pr�fung ob Eingabe nicht l�nger als 3 Zeichen ist
		while(eingabe.length() > 3) {
			System.out.println("Ihre Eingabe war zu lang.");
			System.out.print("Geben Sie bitte drei Buchstaben/Zahlen in beliebiger Reihenfolge: ");
			eingabe = scan.next();
		}
		
		// Speicherung einzelner Zeichen
		char erstesZeichen = eingabe.charAt(0);
		char zweitesZeichen = eingabe.charAt(1);
		char drittesZeichen = eingabe.charAt(2);
		
		// ASCII-Wert eingegebener Zeichen
		final int asciiWert1 = (int)erstesZeichen;
		final int asciiWert2 = (int)zweitesZeichen;
		final int asciiWert3 = (int)drittesZeichen;
		
		// Aufgabe der Zeichen vom kleinsten zum gr��ten Wert
		if(asciiWert1 <= asciiWert2 && asciiWert1 <= asciiWert3) {
			if(asciiWert2 <= asciiWert3)
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", erstesZeichen, zweitesZeichen, drittesZeichen);
			else
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", erstesZeichen, drittesZeichen, zweitesZeichen);
		}
		else if(asciiWert2 <=  asciiWert1 && asciiWert2 <= asciiWert3) {
			if(asciiWert1 <= asciiWert3)
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", zweitesZeichen , erstesZeichen, drittesZeichen);
			else
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", zweitesZeichen , drittesZeichen, erstesZeichen);
		}
		else {
			if(asciiWert1 <= asciiWert2)
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", drittesZeichen, erstesZeichen, zweitesZeichen);
			else
				System.out.printf("Ihre Eingabe in sortierter Form: %c%c%c\n", drittesZeichen, zweitesZeichen, erstesZeichen);
		}
		
		scan.close();
	}
}

// num + 48