// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 2

import java.util.Scanner;

public class Aufgabe2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char antwort;
		double nettobetrag;
		double bruttobetrag;
		
		System.out.print("Geben Sie bitte den Nettobetrag ein: ");
		nettobetrag = scan.nextDouble();
		
		System.out.print("\nNettowert mit erm��igten Steuersatz von 7 % berechnen?\n"
				+ "Falls nein, wird der vollen Steuersatz von 19 % angewendet.\n"
				+ "Ihre Antwort (j/n): ");
		antwort = scan.next().charAt(0);
		
		if(antwort == 'j')
			bruttobetrag = nettobetrag * 1.07;
		else
			bruttobetrag = nettobetrag * 1.19;
		
		System.out.printf("\nDer Bruttobetrag betr�gt %.2f EUR.", bruttobetrag);
		
		scan.close();
	}
}