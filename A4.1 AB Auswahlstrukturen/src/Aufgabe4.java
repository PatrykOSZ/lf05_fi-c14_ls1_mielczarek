// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 4

import java.util.Scanner;

public class Aufgabe4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double bestellwert, gesamtbetrag;
		double mwst = 0.19;
		double rabatt_1 = 0.1, rabatt_2 = 0.15, rabatt_3 = 0.2;
		
		System.out.print("Geben Sie den Bestellwert ein: ");
		bestellwert = scan.nextDouble();
		
		if(bestellwert <= 100) {
			System.out.printf("Ihr Rabatt betr�gt %d %%.\n", (int)(rabatt_1 * 100));
			gesamtbetrag = (bestellwert * (1 - rabatt_1)) + (bestellwert * mwst);
		}
		else if(bestellwert > 100 && bestellwert <= 500) {
			System.out.printf("Ihr Rabatt betr�gt %d %%.\n", (int)(rabatt_2 * 100));
			gesamtbetrag = (bestellwert * (1 - rabatt_2)) + (bestellwert * mwst);
		}
		else {
			System.out.printf("Ihr Rabatt betr�gt %d %%.\n", (int)(rabatt_3 * 100));
			gesamtbetrag = (bestellwert * (1 - rabatt_3)) + (bestellwert * mwst);
		}
		
		System.out.printf("\nDer Gesamtbetrag ist %.2f EUR.", gesamtbetrag);
		
		scan.close();
	}
}