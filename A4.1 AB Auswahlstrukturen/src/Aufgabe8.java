// 4.1 Einf�hrung Auswahlstrukturen -> Aufgabe 8

import java.util.Scanner;

public class Aufgabe8 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int jahr;
		
		System.out.print("Geben Sie ein beliebiges Jahr ein: ");
		jahr = scan.nextInt();
		
		System.out.println("\n****** INFO ******\nSchaltjahre wurden 45 v.Ch. (-45) eingef�hrt.\n"
				+ "Es galt die Regel, dass jedes 4. Jahr ein Schaltjahr ist.\n\n"
				+ "1582 wurden zwei weitere Regeln eingef�hrt:\n"
				+ "Regel 1: Jahre, die durch 100 Teilbar sind, sind keine Schaltjahre.\n"
				+ "Regel 2: Jahre, die durch 400 teilbar sind, sind Schaltjahre.\n\n"
				+ "Nach diesen Gegebenheiten wird ihr Jahr gepr�ft.\n"
				+ "****** ENDE ******\n");
		
		if(jahr < -45)
			System.out.printf("Das Jahr %d ist kein Schaltjahr. Zu diesen Zeitpunkt habe es keine Schaltjahre.\n", jahr);
		else if(jahr < 1582 && jahr % 4 == 0) 
			System.out.printf("Das Jahr %d ist ein Schaltjahr.\n", jahr);
		else if((jahr % 4 == 0 || jahr % 100 == 0) && jahr % 400 != 0)
			System.out.printf("Das Jahr %d ist ein Schaltjahr.\n", jahr);
		else
			System.out.printf("Das Jahr %d ist kein Schaltjahr.\n", jahr);
		
		scan.close();
	}
}