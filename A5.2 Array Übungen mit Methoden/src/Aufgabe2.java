// 5.2 AB Array �bungen mit Methoden -> Aufgabe 2

public class Aufgabe2 {
	public static void main(String[] args) {
		int[] zahlen = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		int arrLaenge = zahlen.length;
		
		umdrehen(zahlen, arrLaenge);
	}
	
	public static void umdrehen(int[] zahlen, int arrLaenge) {
		int[] umgedrehtZahlen = new int[arrLaenge];
		int maxIndex = arrLaenge - 1;
		for(int i = 0; i <= maxIndex; i++) 
			umgedrehtZahlen[i] = zahlen[maxIndex - i];
			
		ausgabe(umgedrehtZahlen);
	}

	private static void ausgabe(int[] zahlen) {
		for(int i = 0; i < zahlen.length; i++)
			System.out.printf("%d ", zahlen[i]);		
	}
}