// 5.2 AB Array �bungen mit Methoden -> Aufgabe 4

public class Aufgabe4 {
	public static void main(String[] args) {
		double [][] temperaturen = new double[2][2];
		temperaturen[0][0] = 0.0;
		temperaturen[1][0] = 10.0;
		
		umrechnung(temperaturen);
	}

	private static void umrechnung(double[][] temperaturen) {
		System.out.print("---------------------------\n");
		System.out.printf("| %-11s| %-11s|\n", "Fahrenheit", "Celsius");
		for(int i = 0; i < temperaturen.length; i++) {
			temperaturen[i][1] = (5.0 / 9.0) * (temperaturen[i][0] - 32.0);
			System.out.printf("| %7.2f �F | %7.2f �C |\n", temperaturen[i][1], temperaturen[i][0]);
		}
	}
}