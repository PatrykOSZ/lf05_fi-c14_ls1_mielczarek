// 5.2 AB Array �bungen mit Methoden -> Aufgabe 1

public class Aufgabe1 {
	public static void main(String[] args) {
		int[] zahlen = { 2, 4, 6, 8, 10 };
		String ausgabe;
		
		ausgabe = convertArrayToString(zahlen);
		System.out.printf("%s\n", ausgabe);
	}
	
	public static String convertArrayToString(int[] zahlen) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < zahlen.length; i++) {
			sb.append(Integer.toString(zahlen[i]));
			if(i < zahlen.length - 1)
				sb.append(", ");
		}

		return sb.toString();
	}
}