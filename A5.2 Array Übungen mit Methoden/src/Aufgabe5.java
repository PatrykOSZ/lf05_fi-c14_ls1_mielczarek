// 5.2 AB Array �bungen mit Methoden -> Aufgabe 5

import java.util.Scanner;

public class Aufgabe5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char antwort;
		int posX, posY, indexX, indexY, wert;
		int[] matrixGroesse = new int[2];
		String[] fragenMatrixGroesse = {"Wie viele Zeilen sollen erstellt werden? ",
										"Wie viele Spalten sollen erstellt werden? "};
		String[] fragenKoordinaten = new String[3];
		fragenKoordinaten[0] = "M�chten Sie Werte eintragen? (j/n): ";
		
		// Eingabe der Matrix Gr��e -> Index 0 = Zeilen; Index 1 = Spalten
		int arrLaenge = fragenMatrixGroesse.length;
		matrixGroesse = eingabeMatrixGroesse(arrLaenge, fragenMatrixGroesse, scan);
		
		// Matrix wird erstellt -> Index 0 = Zeilen; Index 1 = Spalten
		int[][] meinMatrix = new int[matrixGroesse[1]][matrixGroesse[0]];
		
		// Erstellen von Fragen
		fragenKoordinaten[1] = "Welche Zeile? (max. " + matrixGroesse[0] + "): ";
		fragenKoordinaten[2] = "Welche Spalte? (max. " + matrixGroesse[1] + "): ";
		
		// Werte in die Matrix eintragen
		antwort = eingabePruefung(fragenKoordinaten, scan);
		while(antwort == 'j') {
			posY = koordinaten(fragenKoordinaten[1], scan);
			indexY = posY - 1;
			posX = koordinaten(fragenKoordinaten[2], scan);
			indexX = posX - 1;
			
			// Einlesen der Eingabe in die Matrix-Zellen
			System.out.printf("Tragen Sie ein Wert ein in die Zeile %d Spalte %d ein: ", posY, posX);
			wert = scan.nextInt();
			meinMatrix = matrixBefuellen(meinMatrix, indexX, indexY, wert);
		}
		
		// Matrix ausgeben
		System.out.print("M�chten Sie die Tabelle ausgeben? (j/n): ");
		antwort = scan.next().charAt(0);
		if(antwort == 'j') {			
			for(int j = 0; j < meinMatrix.length; j++) {
				for(int i = 0; i < (meinMatrix[0].length * 8); i++)
					System.out.print("-");
				
				System.out.print("-\n");
				
				for(int k = 0; k < meinMatrix[0].length; k++) {
					System.out.printf("| %5d ", meinMatrix[j][k]);
					if(k == meinMatrix[0].length - 1)
						System.out.print("|");
				}
				System.out.print("\n");
			}
		}
		
		scan.close();
	}

	private static int[] eingabeMatrixGroesse(int arrLaenge, String[] frage, Scanner scan) {
		int[] eingabe = new int[2];
		for(int i = 0; i < arrLaenge; i++) {
			System.out.printf("%s", frage[i]);
			eingabe[i] = scan.nextInt();
		}
		return eingabe;
	}

	private static char eingabePruefung(String[] fragenKoordinaten, Scanner scan) {
		boolean loop = true;
		char antwort = ' ';
		while(loop) {
			System.out.print(fragenKoordinaten[0]);
			antwort = scan.next().charAt(0);
			if(antwort == 'n' || antwort == 'j')
				loop = false;
		}
		return antwort;
	}
	
	private static int koordinaten(String fragenKoordinaten, Scanner scan) {
		System.out.printf("%s", fragenKoordinaten);
		return scan.nextInt();
	}
	
	private static int[][] matrixBefuellen(int[][] meinMatrix, int indexX, int indexY, int wert) {
		meinMatrix[indexY][indexX] = wert;
		return meinMatrix;
		
	}
}