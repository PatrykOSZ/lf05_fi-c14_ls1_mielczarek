import java.util.Scanner;

public class Aufgabe6 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char eingabe;
		do {
			System.out.print("\nWie viel Kapital (in Euro) m�chten Sie anlegen: ");
			double startKapital = scan.nextDouble();
			System.out.print("Zinssatz: ");
			double zins = scan.nextDouble();
			
			int jahre = 0;
			double summe = startKapital;
			while(summe < 1000000) {
				summe += (summe * (zins / 100));
				jahre++;
			}
			
			System.out.printf("Sie werden in %d Jahren �ber eine Million Euro haben.\n", jahre);
			System.out.print("M�chten Sie noch eine Rechnung vornehmen? (j/n) ");
			eingabe = scan.next().charAt(0);
		}while(eingabe == 'j');
		
		System.out.print("Bis zum n�chsten Mal.\n");
		
		scan.close();
	}

}
