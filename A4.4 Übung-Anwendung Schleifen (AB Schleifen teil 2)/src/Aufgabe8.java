
public class Aufgabe8 {
	public static void main(String[] args) {
		int summe = 0, temp;
		for(int i = 0; i < 100; i++) {
			temp = i;
			while(0 != temp) {
				summe += (temp % 10);
				temp /= 10;
			}
			
			if(i % 4 == 0 && i != 0)
				System.out.printf("%4s", "*");
			else if(summe == 4 && i != 0)
				System.out.printf("%4s", "*");
			else
				System.out.printf("%4d", i);
			
			summe = 0;
			
			if((i + 1) % 10 == 0 && i != 0)
				System.out.print("\n");
		}
	}
}
