
public class Aufgabe7 {
	public static void main(String[] args) {
		double sprinterA = 9.7, sprinterB = 7;
		int strecke = 1000, vorsprung = 250;
		
		int sekunden = 0;
		double distanzA = 0.0, distanzB = vorsprung;
		System.out.print(" Sekunden | Sprinter A | Sprinter B |\n");
		while(distanzA <= strecke && distanzB <= strecke) {
			sekunden++;
			distanzA += sprinterA;
			distanzB += sprinterB;
			System.out.printf(" %8d | %10.2f | %10.2f |\n", sekunden, distanzA, distanzB);
		}
	}
}