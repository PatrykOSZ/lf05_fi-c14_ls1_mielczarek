import java.util.Scanner;

public class Aufgabe4 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		double startwert = scan.nextDouble();
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		double endwert = scan.nextDouble();
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schritte = scan.nextDouble();
		
		double fahrenheit;
		for(double celsius = startwert; celsius <= endwert; celsius += schritte) {
			fahrenheit = (celsius * 1.8) + 32;
			System.out.printf("%6.2f%-5s %6.2f%-5s\n", celsius, "�C", fahrenheit, "�F");
		}
	}

}
