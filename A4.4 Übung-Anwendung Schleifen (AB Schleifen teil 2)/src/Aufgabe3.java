import java.util.Scanner;

public class Aufgabe3 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		int zahl = eingabe();
		System.out.printf("Die Quersumme betr�gt: %d.", quersumme(zahl));
	}

	public static int eingabe() {
		System.out.print("Geben sie bitte eine Zahl ein: ");
		return scan.nextInt();
	}
	
	public static int quersumme(int zahl) {
		int summe = 0;
		while(0 != zahl) {
			summe += (zahl % 10);
			zahl /= 10;
		}
		
		return summe;
	}
}