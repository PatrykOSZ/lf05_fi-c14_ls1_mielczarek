import java.util.Scanner;

public class Aufgabe5 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		int laufzeit = scan.nextInt();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double startKapital = scan.nextDouble();
		System.out.print("Zinssatz: ");
		double zins = scan.nextDouble();
		
		double summeKapital = startKapital;
		for(int i = 0; i < laufzeit; i++) {
			summeKapital += (summeKapital * (zins / 100));
		}
		
		System.out.printf("\nEingezahltes Kapital: %.2f\n", startKapital);
		System.out.printf("Ausgezahltes Kapital: %.2f\n", summeKapital);
	}
}
