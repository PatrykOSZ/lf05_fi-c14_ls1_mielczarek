import java.util.Scanner;

public class Aufgabe1 {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		int n = eingabe();
		char modus = eingabeModus();
		counter(n, modus);
	}

	public static int eingabe() {
		System.out.print("Geben Sie eine Zahl ein: ");
		return scan.nextInt();
	}
	
	public static char eingabeModus() {
		char input;
		do {
			System.out.print("Z�hlmodus ausw�rts (+) oder abw�rts (-)? ");
			input = scan.next().charAt(0);
		}while(input != '+' && input != '-');
		
		return input;
	}
	
	private static void counter(int n, char modus) {
		if(modus == '+') {
			for(int i = 0; i <= n; i++) {
				if(i != n)
					System.out.printf("%d, ", i);
				else
					System.out.printf("%d", i);
			}
		}
		else {
			for(int i = n; i >= 0; i--) {
				if(i != 0)
					System.out.printf("%d, ", i);
				else
					System.out.printf("%d", i);
			}
		}
	}
}