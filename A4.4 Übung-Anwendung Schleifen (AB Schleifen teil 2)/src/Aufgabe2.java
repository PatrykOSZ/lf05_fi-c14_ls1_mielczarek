
public class Aufgabe2 {
	public static void main(String[] args) {
		int n = 5;
		int x = fakultaet(n);
		
		System.out.printf("Fakult�t zu %d ist %d", n, x);
	}

	public static int fakultaet(int n) {
		int x = 1;
		for(int i = 1; i <= n; i++)
			x *= i;
		
		return x;
	}

}
