
public class Aufgabe5 {
	public static void main(String[] args) {
		einsMalEins();
	}

	private static void einsMalEins() {
		for(int i = 1; i <= 10; i++) {
			for(int j = 1; j <= 10; j++) {
				System.out.printf("%4d |", j * i);
			}
			System.out.print("\n");
		}
	}
}