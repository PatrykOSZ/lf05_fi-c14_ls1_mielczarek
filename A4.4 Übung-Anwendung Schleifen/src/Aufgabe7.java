
public class Aufgabe7 {
	public static void main(String[] args) {
		primzahlen(100);
	}
	
	public static void primzahlen(int x) {
		boolean Prim = true;
		for(int i = 1; i <= x; i++) {
			for (int j = 2; j < i-1; j++){
				if (i%j == 0)
					Prim = false;
			}
			
			if (Prim)
				System.out.printf("%d, ", i);
			else
				Prim = true;
		}
	}
}