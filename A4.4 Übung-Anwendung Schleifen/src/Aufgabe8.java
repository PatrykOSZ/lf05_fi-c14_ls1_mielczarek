
public class Aufgabe8 {
	public static void main(String[] args) {
		quadrat(5);
	}

	public static void quadrat(int amount) {
		for(int i = 0; i < amount; i++) {
			for(int j = 0; j < amount; j++) {
				if(i == 0 || i == amount - 1)
					System.out.print("*");
				else if (j == 0 || j == amount - 1)
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.print("\n");
		}
	}
}