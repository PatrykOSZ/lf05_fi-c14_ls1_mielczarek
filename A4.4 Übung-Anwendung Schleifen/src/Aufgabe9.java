
public class Aufgabe9 {
	public static void main(String[] args) {
		int h = 4, b = 3;
		treppen(h, b);
	}

	public static void treppen(int h, int b) {
		int x = 0;
		for(int i = 0; i < h; i++) {
			x += b;
			for(int j = 0; j < x; j++) {
				System.out.print("*");
			}
			System.out.print("\n");
		}
	}
}