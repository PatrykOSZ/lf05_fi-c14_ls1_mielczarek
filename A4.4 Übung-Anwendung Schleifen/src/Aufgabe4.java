
public class Aufgabe4 {
	public static void main(String[] args) {
		schreife_1();
		schreife_2();
		schreife_3();
		schreife_4();
		schleife_5();
	}

	private static void schreife_1() {
		System.out.print("a) ");
		for(int i = 99; i > 0; i -= 3)
			System.out.printf("%d, ", i);
		
		System.out.print("\n");
	}

	private static void schreife_2() {
		System.out.print("b) ");
		int temp = 0;
		for(int i = 0; temp < 400; i++) {
			temp = (int)Math.pow(i, 2);
			System.out.printf("%d, ", temp);
		}
		
		System.out.print("\n");
	}

	private static void schreife_3() {
		System.out.print("c) ");
		int temp = 0;
		for(int i = 0; temp < 102; i++) {
			temp = (4 * i) + 2;
			System.out.printf("%d, ", temp);
		}
		
		System.out.print("\n");
	}

	private static void schreife_4() {
		System.out.print("d) ");
		int temp = 0;
		for(int i = 1; temp < 1024; i++) {
			temp = (int)Math.pow((2*i), 2);
			System.out.printf("%d, ", temp);
		}
		
		System.out.print("\n");
	}

	public static void schleife_5() {
		System.out.print("e) ");
		int temp = 0;
		for(int i = 1; temp < 35000; i++) {
			temp = (int)Math.pow(2, i);
			System.out.printf("%d, ", temp);
		}
	}
}