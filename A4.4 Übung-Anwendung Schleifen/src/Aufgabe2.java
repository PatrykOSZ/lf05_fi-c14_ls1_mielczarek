import java.util.Scanner;

public class Aufgabe2 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		int n = eingabe();
		summe(n);
		doppelteSumme(n);
		ungerade(n);
	}

	public static int eingabe() {
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		return scan.nextInt();
	}

	public static void summe(int n) {
		int temp = 0;
		for(int i = 0; i <= n; i++)
			temp += i;
		
		ausgabe('A', temp);
	}
	
	public static void doppelteSumme(int n) {
		int temp = 0;
		for(int i = 0; i <= n; i++) 
			temp += i * 2;
		
		ausgabe('B', temp);
	}
	
	public static void ungerade(int n) {
		int temp = 0;
		for(int i = 0; i <= n; i++)
			temp += (i * 2) + 1;
		
		ausgabe('C', temp);
	}
	
	public static void ausgabe(char typ, int temp) {
		System.out.printf("Die Summe f�r %c betr�gt: %d\n", typ, temp);
	}
}