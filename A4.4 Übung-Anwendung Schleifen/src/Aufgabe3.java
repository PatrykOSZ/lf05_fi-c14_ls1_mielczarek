
public class Aufgabe3 {
	public static void main(String[] args) {
		int x = 7, y = 5, z = 4;
		teilbarkeit(x, y, z);
	}

	private static void teilbarkeit(int x, int y, int z) {
		for(int i = 1; i <= 200; i++) {
			if(i % x == 0 && i % y != 0 &&i % z == 0)
				System.out.printf("%d, ", i);
		}
	}
}
