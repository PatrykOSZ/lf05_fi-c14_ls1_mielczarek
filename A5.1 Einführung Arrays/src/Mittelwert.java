import java.util.Scanner;

public class Mittelwert {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		double m;
		
		m = berechneMittelwert(eingabe(), eingabe(), 2);
		ausgabe(m);
		
		while(true) {
			m = berechneMittelwert(m, eingabe(), 2);
			ausgabe(m);
		}
	}

	public static double eingabe() {
		System.out.print("Geben Sie eine Zahl ein: ");
		return scan.nextDouble();
	}

	public static double berechneMittelwert(double m, double b, int i) {
		return (m + b) / i;
	}
	
	public static void ausgabe(double m) {
		System.out.printf("Der Mittelwert ist %.2f\n", m);
	}
}