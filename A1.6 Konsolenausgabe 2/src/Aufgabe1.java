// A1.6 Ausgabeformatierung 2 -> Aufgabe 1

public class Aufgabe1 {
	public static void main(String[] args) {
		// Formatierte Ausgabe eines Kreises
		System.out.printf("%5s\n", 	"**");
		System.out.printf("%c", 	'*');
		System.out.printf("%7s\n", 	"*");
		System.out.printf("%c", 	'*');
		System.out.printf("%7s\n", 	"*");
		System.out.printf("%5s\n", 	"**");
	}
}