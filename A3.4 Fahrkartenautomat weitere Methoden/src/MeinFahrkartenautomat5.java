// A2.5 Anzahl der Tickets -> Aufgabe 5

import java.util.Scanner;

public class MeinFahrkartenautomat5 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {		 

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);	
		fahrkartenAusgaben();
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		// Informationstext
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir w�nschen Ihnen eine gute Fahrt.");
   
		scan.close();
	}

	public static double fahrkartenbestellungErfassen(){
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = scan.nextDouble();
		
		System.out.print("Anzahl der Tickets: ");
		int anzahlTickets = scan.nextInt();
		
		return zuZahlenderBetrag *= anzahlTickets;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = scan.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgaben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}
	
	private static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if(r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while(r�ckgabebetrag >= 2.0) {// 2 EURO-M�nzen
				muenzeAusgeben(2, "EURO");
				r�ckgabebetrag -= 2.0;
			}
			while(r�ckgabebetrag >= 1.0) { // 1 EURO-M�nzen
				muenzeAusgeben(1, "EURO");
				r�ckgabebetrag -= 1.0;
			}
			while(r�ckgabebetrag >= 0.5) { // 50 CENT-M�nzen
				muenzeAusgeben(50, "CENT");
				r�ckgabebetrag -= 0.5;
			}
			while(r�ckgabebetrag >= 0.2) { // 20 CENT-M�nzen
				muenzeAusgeben(20, "CENT");
				r�ckgabebetrag -= 0.2;
			}
			while(r�ckgabebetrag >= 0.1) { // 10 CENT-M�nzen
				muenzeAusgeben(10, "CENT");
				r�ckgabebetrag -= 0.1;
			}
			while(r�ckgabebetrag >= 0.05) { // 5 CENT-M�nzen
				muenzeAusgeben(5, "CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.printf("%7s\n", "****");
		System.out.printf("%2s", "*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%s", "*");
		System.out.printf("%2s %s", betrag, einheit);
		System.out.printf("%2s\n", "*");
		System.out.printf("%2s", "*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%7s\n", "****");
	}
}