/*
 * Aufgabe:  Recherechieren Sie im Internet !
 * Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
 * Vergessen Sie nicht den richtigen Datentyp !!
 * 
 * @version 1.0 from 21.08.2019
 * @author Patryk Mielczarek
 */

public class WeltDerZahlen {
	public static void main(String[] args) {
    /* *********************************************************
     * Zuerst werden die Variablen mit den Werten festgelegt!
     * ********************************************************* */
    // Im Internet gefunden 						-> Wikipedia
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 800000000000L;
    
    // Wie viele Einwohner hat Berlin? 				-> 3,664 Millionen Stand: November 2021
    int bewohnerBerlin = 3664000;
    
    // Wie alt bist du?  Wie viele Tage sind das? 	-> Stand 6.November 2021
    int alter = 31;
    int alterTage = 11688;
    
    // Wie viel wiegt das schwerste Tier der Welt? 	-> Riesen Blauwal 200 Tonnen
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat? -> Russland
    int flaecheGroessteLand = 17098246;
    
    // Wie gro� ist das kleinste Land der Erde?		-> Vatikanstaat
    float flaecheKleinsteLand = 0.44F;
    
    /*  *********************************************************
    Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    System.out.println(" *******  Anfang des Programms  ******* ");
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
    System.out.println("Mein Alter: " + alter);
    System.out.println("Anzahl der Tage von meiner Geburt: " + alterTage);
    System.out.println("Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm + " kg");
    System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand + " �km");
    System.out.println("Fl�che des kleinsten Landes der Welt: " + flaecheKleinsteLand + " �km");
    System.out.println(" *******  Ende des Programms  ******* ");
    }
}
