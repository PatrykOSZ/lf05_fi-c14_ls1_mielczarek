/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author:	Patryk Mielczarek
    @version:	1.0 ?
*/
public class Variablen {
	public static void main(String [] args){
	    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine geeignete Variable */
	    /* 2. Weisen Sie dem Zaehler den Wert 25 zu und geben Sie ihn auf dem Bildschirm aus.*/
	    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable */
	    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu und geben Sie ihn auf dem Bildschirm aus.*/
	    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte notwendig. Vereinbaren Sie eine geeignete Variable */
	    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu und geben Sie sie auf dem Bildschirm aus.*/
	    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung soll die Anzahl der Mitglieder erfasst werden. Vereinbaren Sie eine geeignete Variable und initialisieren sie diese sinnvoll.*/
	    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt. Vereinbaren Sie eine geeignete Variable und geben Sie sie auf dem Bildschirm aus.*/
	    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist. Vereinbaren Sie eine geeignete Variable. */
	    /*11. Die Zahlung ist erfolgt. Weisen Sie der Variable den entsprechenden Wert zu und geben Sie die Variable auf dem Bildschirm aus.*/
			
		int zaehler = 25;							// Aufgabe 1. und 2.
		char eingabe = 'C';							// Aufgabe 3. und 4.
		int lichtgeschwindigkeit = 299792458;		// Aufgabe 5. und 6.
		int mitgliederAnzahl = 7;					// Aufgabe 7.
		double elementarladung = 5.5;				// Aufgabe 9.		
		boolean istBezahlt = true;					// Aufgabe 10.
		
		System.out.println("Anzahl der Programmdurchläufe: " + zaehler);				// Aufgabe 2.
		System.out.println("Eingegebener Buchstabe: " + eingabe);						// Aufgabe 4.
		System.out.println("Lichtgeschwindigkeit: " + lichtgeschwindigkeit + " m/s");	// Aufgabe 6.
		System.out.println("Anzahl der Vereinsmitglieder: " + mitgliederAnzahl);		// Aufgabe 8.
		System.out.println("Elementarladungsstärke: " + elementarladung + " e");		// Aufgabe 9.
		System.out.println("Zahlung bezahlt?: " + istBezahlt);							// Aufgabe 11.
	}//main
}// Variablen