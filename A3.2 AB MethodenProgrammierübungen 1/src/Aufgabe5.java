import java.util.Scanner;

public class Aufgabe5 {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		double x, result;
		
		x = eingabe();	
		result = quadrat(x);
		ausgabe(x, result);
	}
	
	public static double eingabe() {
		System.out.printf("Geben Sie einen Wert ein: ");
		return scan.nextDouble();
	}
	
	public static double quadrat(double x) {
		return Math.pow(x, 2);
	}

	public static void ausgabe(double x, double result){
		System.out.printf("%.2f x %.2f = %.2f\n", x, x, result);
	}
}
