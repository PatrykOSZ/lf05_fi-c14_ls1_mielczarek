// 3.2 AB MethodenProgrammierübungen 1 -> Aufgabe 4

import java.util.Scanner;

public class Aufgabe4 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		double r1, r2, rGes;
		
		r1 = eingabe(1);
		r2 = eingabe(2);;
		
		rGes = parallelschaltung(r1, r2);
		ausgabe(r1, r2, rGes);
		
		scan.close();
	}
	
	public static double eingabe(int num) {
		System.out.printf("Geben Sie den Wert für \"r%d\" ein: ", num);
		return scan.nextDouble();
	}
	
	public static double parallelschaltung(double r1, double r2){
		return 1/r1 + 1/r2;
	}
	
	public static void ausgabe(double r1, double r2, double rGes){
		System.out.printf("1 / %.2f + 1 / %.2f = %.2f\n", r1, r2, rGes);
	}
}