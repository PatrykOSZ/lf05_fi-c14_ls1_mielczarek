import java.util.Scanner;

public class Aufgabe6 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		char weiterFahren = 'j';
		double v = 0.0, dv;

		while(weiterFahren == 'j') {
			dv = eingabe();
			v = beschleunige(v, dv);
			
			if(v >= 130)
				v = 130;
			else if(v <= 0)
				v = 0;
			
			weiterFahren = ausgabe(v);
		}
	}

	public static double eingabe() {
		System.out.print("Um wie viel km/h soll beschleunigt/abgebremst werden?: ");
		return scan.nextDouble();
	}

	public static double beschleunige(double v, double dv) {
		return v + dv;
	}

	public static char ausgabe(double v) {
		System.out.printf("Sie fahren %.1f km/h\nWeiter fahren (j/n)?: ", v);
		return scan.next().charAt(0);
	}
}
