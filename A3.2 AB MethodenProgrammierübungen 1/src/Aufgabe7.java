import java.util.Scanner;

public class Aufgabe7 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		double k1, k2, h;
		
		k1 = eingabe(1);
		k2 = eingabe(2);
		
		k1 = quadrat(k1);
		k2 = quadrat(k2);
		
		h = hypotenuse(k1, k2);
		
		ausgabe(h);
	}

	public static double eingabe(int anzahl) {
		System.out.printf("Geben Sie Gr��e der %d-ten Kathete ein: ", anzahl);
		return scan.nextDouble();
	}
	
	private static double quadrat(double kathete) {
		return Math.pow(kathete, 2);
	}

	private static double hypotenuse(double k1, double k2) {
		return Math.sqrt(quadrat(k1) + quadrat(k2));
	}
	
	private static void ausgabe(double h) {
		System.out.printf("Die Hypotenuse ist %.2f.", h);
	}
}
