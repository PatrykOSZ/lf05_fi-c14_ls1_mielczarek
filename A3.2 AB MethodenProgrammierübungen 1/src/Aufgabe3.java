// 3.2 AB MethodenProgrammierübungen 1 -> Aufgabe 3

import java.util.Scanner;

public class Aufgabe3 {
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		double r1, r2, rGes;
		
		r1 = eingabe(1);
		r2 = eingabe(2);;
		
		rGes = reihenschaltung(r1, r2);
		ausgabe(r1, r2, rGes);
		
		scan.close();
	}
	
	public static double eingabe(int num) {
		System.out.printf("Geben Sie den Wert für \"r%d\" ein: ", num);
		return scan.nextDouble();
	}
	
	public static double reihenschaltung(double r1, double r2){
		return r1 + r2;
	}
	
	public static void ausgabe(double r1, double r2, double rGes){
		System.out.printf("%.2f + %.2f = %.2f\n", r1, r2, rGes);
	}
}