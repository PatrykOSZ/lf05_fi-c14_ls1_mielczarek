// 3.2 AB MethodenProgrammierübungen 1 -> Aufgabe 2

import java.util.Scanner;

public class Aufgabe2 {
	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		
		int anzahl;
		double nettopreis, nettogesamtpreis, mwst, gesamtbruttopreis;
		String textArtikel = "Was möchten Sie bestellen? ";
		String textAnzahl = "Geben Sie die Anzahl ein: ";
		String textNettopreis = "Geben Sie den Nettopreis ein: ";
		String artikel;
		
		artikel = liesString(textArtikel);
		anzahl = liesInt(textAnzahl);
		nettopreis = liesDouble(textNettopreis);
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst = myScanner.nextDouble();
		
		gesamtbruttopreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		rechungausgeben(artikel, anzahl, nettogesamtpreis, gesamtbruttopreis, mwst);
		
		myScanner.close();
	}
	
	public static String liesString(String text) {
		System.out.print(text);
		return myScanner.next();
	}
	
	public static int liesInt(String text) {
		System.out.print(text);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String text) {
		System.out.print(text);
		return myScanner.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
