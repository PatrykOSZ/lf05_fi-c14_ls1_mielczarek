// 4.1 Einf�hrung Fallunterscheidungen -> Aufgabe 4

import java.text.DecimalFormat;
import java.util.Scanner;

public class Aufgabe4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat format = new DecimalFormat("#.######");
		
		char mathZeichen;
		double zahl1, zahl2, ergebnis = 0;
		
		System.out.print("Geben Sie erste Zahl ein: ");
		zahl1 = scan.nextInt();
		System.out.print("Geben Sie zweite Zahl ein: ");
		zahl2 = scan.nextInt();
		System.out.print("G�ltige Operatoren\n"
				+ "     Addition:       +\n"
				+ "     Subtraktion:    -\n"
				+ "     Multiplikation: *\n"
				+ "     Division:       /\n"
				+ "\nGeben Sie ein Oparator ein: ");
		mathZeichen = scan.next().charAt(0);
		
		if(mathZeichen == '+')
			ergebnis = zahl1 + zahl2;
		else if(mathZeichen == '-')
			ergebnis = zahl1 - zahl2;
		else if(mathZeichen == '*')
			ergebnis = zahl1 * zahl2;
		else if(mathZeichen == '/')
			ergebnis = zahl1 / zahl2;
		else
			System.out.println("Ihre Eingabe ist kein g�ltiger Operator.\n");
		
		System.out.printf("%s %c %s = %s\n", format.format(zahl1), mathZeichen, format.format(zahl2), format.format(ergebnis));
		
		scan.close();
	}
}