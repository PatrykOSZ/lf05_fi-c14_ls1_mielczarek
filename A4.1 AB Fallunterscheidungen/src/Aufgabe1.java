// 4.1 Einführung Fallunterscheidungen -> Aufgabe 1

import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int note;
		
		System.out.print("Geben Sie Ihre Note ein: ");
		note = scan.nextInt();
		
		if(note == 1)
			System.out.print("Ihre note in Schriftform: \"Sehr gut\".\n");
		else if(note == 2)
			System.out.print("Ihre note in Schriftform: \"Gut\".\n");
		else if(note == 3)
			System.out.print("Ihre note in Schriftform: \"Befriedigend\".\n");
		else if(note == 4)
			System.out.print("Ihre note in Schriftform: \"Ausreichend\".\n");
		else if(note == 5)
			System.out.print("Ihre note in Schriftform: \"Mangelhaft\".\n");
		else if(note == 6)
			System.out.print("Ihre note in Schriftform: \"Ungenügend\".\n");
		else
			System.out.print("Ihre Eingabe ist keine Note.\n");
		
		scan.close();
	}
}