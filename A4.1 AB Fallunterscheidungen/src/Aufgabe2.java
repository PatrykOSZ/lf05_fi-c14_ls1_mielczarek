// 4.1 Einf�hrung Fallunterscheidungen -> Aufgabe 2

import java.util.Scanner;

public class Aufgabe2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int monat;
		
		System.out.print("Geben Sie ein Monat als Zahl ein: ");
		monat = scan.nextInt();
		
		if(monat == 1)
			System.out.print("Der Monat hei�t Januar.\n");
		else if(monat == 2)
			System.out.print("Der Monat hei�t Februar.\n");
		else if(monat == 3)
			System.out.print("Der Monat hei�t M�rz.\n");
		else if(monat == 4)
			System.out.print("Der Monat hei�t April.\n");
		else if(monat == 5)
			System.out.print("Der Monat hei�t Mai.\n");
		else if(monat == 6)
			System.out.print("Der Monat hei�t Juni.\n");
		else if(monat == 7)
			System.out.print("Der Monat hei�t Juli.\n");
		else if(monat == 8)
			System.out.print("Der Monat hei�t August.\n");
		else if(monat == 9)
			System.out.print("Der Monat hei�t September.\n");
		else if(monat == 10)
			System.out.print("Der Monat hei�t Oktober.\n");
		else if(monat == 11)
			System.out.print("Der Monat hei�t November.\n");
		else if(monat == 12)
			System.out.print("Der Monat hei�t Dezember.\n");
		else
			System.out.print("Ihre Eingabe ist kein Monat.\n");
		
		scan.close();
	}
}