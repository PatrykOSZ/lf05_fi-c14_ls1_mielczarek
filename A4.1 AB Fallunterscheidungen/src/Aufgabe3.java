// 4.1 Einf�hrung Fallunterscheidungen -> Aufgabe 3

import java.util.Scanner;

public class Aufgabe3 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char roemischeZahl;
		
		System.out.print("Geben Sie eine r�mische Zahl ein (I/V/X/L/C/D/M): ");
		roemischeZahl = scan.next().charAt(0);
		
		roemischeZahl = Character.toUpperCase(roemischeZahl);
		
		if(roemischeZahl == 'I')
			System.out.print("Der Wert der Zahl: 1.\n");
		else if(roemischeZahl == 'V')
			System.out.print("Der Wert der Zahl: 5.\n");
		else if(roemischeZahl == 'X')
			System.out.print("Der Wert der Zahl: 10.\n");
		else if(roemischeZahl == 'L')
			System.out.print("Der Wert der Zahl: 50.\n");
		else if(roemischeZahl == 'C')
			System.out.print("Der Wert der Zahl: 100.\n");
		else if(roemischeZahl == 'D')
			System.out.print("Der Wert der Zahl: 500.\n");
		else if(roemischeZahl == 'M')
			System.out.print("Der Wert der Zahl: 1000.\n");
		else
			System.out.print("Ihre Eingabe ist keine r�mische Zahl.\n");
		
		scan.close();
	}
}