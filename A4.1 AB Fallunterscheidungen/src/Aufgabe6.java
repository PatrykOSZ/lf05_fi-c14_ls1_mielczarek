// 4.1 Einf�hrung Fallunterscheidungen -> Aufgabe 6

import java.util.Scanner;

public class Aufgabe6 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int strLen, wiederholung = 0, ergebnis = 0;
		String roemischeZahl;
		
		System.out.print("Geben Sie eine zusammengesetzte r�mische Zahl ein: ");
		roemischeZahl = scan.next();
		
		strLen = roemischeZahl.length();
		
		// Pr�fung ob eingegebene Zeichen r�mische Zahlen sind
		for(int i = 0; i < strLen; i++) {
			if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'I' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'V'
				|| Character.toUpperCase(roemischeZahl.charAt(i)) == 'X' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'L' 
				|| Character.toUpperCase(roemischeZahl.charAt(i)) == 'C' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'D' 
				|| Character.toUpperCase(roemischeZahl.charAt(i)) == 'M')
				continue;
			else {
				System.out.printf("Sie haben das Zeichen \"%c\" eingegeben, welches keine r�mische Zahl ist.\n", roemischeZahl.charAt(i));
				System.exit(0);
			}
		}
		
		// Pr�fung ob ein Zeichen mehr als 3 mal eingegeben wurde
		for(int i = 0; i < strLen; i++) {
			if(i + 1 < strLen) {
				if(roemischeZahl.charAt(i) == roemischeZahl.charAt(i + 1)) {
					wiederholung++;
					if(wiederholung >= 3) {
						System.out.printf("Sie haben \"%c\" mehr als 3 Mal eingegeben. Berechnung wird Unterbrochen.\n", roemischeZahl.charAt(i));
						System.exit(0);
					}
				}
				else
					wiederholung = 0;
			}
		}
		
		// Addition mit Subtraktionsregel der r�mischen Zeichen
		for(int i = 0; i < strLen; i++) {
			if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'I') {
				if(i + 1 != '\0') {
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'I')
						ergebnis += 1;
					else if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'V' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'X')
						ergebnis -= 1;
					else {
						System.out.print("Falsche Reihenfolge der r�mischen Zahlen!\n");
						System.exit(0);
					}
				}
				else
					ergebnis += 1;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'V') {
				if(i + 1 != '\0') 
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) != 'I' || Character.toUpperCase(roemischeZahl.charAt(i + 1)) != 'V')
						System.out.print("Falsche Reihenfolge der r�mischen Zahlen!\n");
				else
					ergebnis += 5;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'X'){
				if(i + 1 != '\0') {
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'I' || Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'V'
						|| Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'X')
						ergebnis += 10;
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'L' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'C')
						ergebnis -= 10;
					else 
						System.out.print("Falsche Reihenfolge der r�mischen Zahlen!\n");
				}
				else
					ergebnis += 10;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'L'){
				if(i + 1 != '\0') {
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'I' || Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'V'
						|| Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'X' || Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'L')
						ergebnis += 50;
					else
						System.out.print("Falsche Reihenfolge der r�mischen Zahlen!\n");
				}					
				else
					ergebnis += 50;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'C') {
				if(i + 1 != '\0') {
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'D' || Character.toUpperCase(roemischeZahl.charAt(i)) == 'M')
						ergebnis -= 100;
					else
						ergebnis += 100;
				}
				else
					ergebnis += 100;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'D') {
				if(i + 1 != '\0') {
					if(Character.toUpperCase(roemischeZahl.charAt(i + 1)) == 'M')
						System.out.print("Falsche Reihenfolge der r�mischen Zahlen!\n");
					else
						ergebnis += 500;
				}
				ergebnis += 500;
			}
			else if(Character.toUpperCase(roemischeZahl.charAt(i)) == 'M')
				ergebnis += 1000;
			else
				System.out.println("Ihre Eingabe ist keine r�mische Zahl.");
		}
			
		
		System.out.printf("Die r�mische Zahl \"%s\" ist %d.\n", roemischeZahl, ergebnis);
		
		scan.close();
	}
}