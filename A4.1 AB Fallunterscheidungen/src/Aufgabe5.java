// 4.1 Einf�hrung Fallunterscheidungen -> Aufgabe 5

import java.text.DecimalFormat;
import java.util.Scanner;

public class Aufgabe5 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat format = new DecimalFormat();
		char physikalischeGroesse;
		double r, u, i, ergebnis;
		
		System.out.print("Geben Sie eine physikalische Gr��e ein (R/U/I): ");
		physikalischeGroesse = scan.next().charAt(0);
		
		physikalischeGroesse = Character.toUpperCase(physikalischeGroesse);
		
		if(physikalischeGroesse == 'R') {
			System.out.print("Geben Sie die Gr��e der Spannung (U) in Volt ein: ");
			u = scan.nextDouble();
			System.out.print("Geben Sie die Gr��e des Stroms (I) in Ampere ein: ");
			i = scan.nextDouble();
			
			ergebnis = u / i;
			
			System.out.printf("Der Wiederstand (R) ist %s Ohm gro�.\n", format.format(ergebnis));
		}
		else if(physikalischeGroesse == 'U') {
			System.out.print("Geben Sie die Gr��e des Wiederstandes (R) in Ohm ein: ");
			r = scan.nextDouble();
			System.out.print("Geben Sie die Gr��e der Strom (I) in Ampere ein: ");
			i = scan.nextDouble();
			
			ergebnis = r * i;
			
			System.out.printf("Die Spannung (U) ist %s Volt gro�.\n", format.format(ergebnis));
		}
		else if(physikalischeGroesse == 'I') {
			System.out.print("Geben Sie die Gr��e der Spannung (U) in Volt ein: ");
			u = scan.nextDouble();
			System.out.print("Geben Sie die Gr��e des Wiederstandes (R) in Ohm ein: ");
			r = scan.nextDouble();
			
			ergebnis = u / r;
			
			System.out.printf("Der Strom (I) ist %s Ampere gro�.\n", format.format(ergebnis));
		}
		else
			System.out.println("Ihre Eingabe ist keine physikalische Gr��e.\n");
		
		scan.close();
	}
}