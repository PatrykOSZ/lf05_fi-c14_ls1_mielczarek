// A2.2 Konsoleneingabe -> Aufgabe 1

import java.util.Scanner;

public class Rechner {
	public static void main(String[] args) {
		// Scanner Initialisation
		Scanner myScanner = new Scanner(System.in); 
		
		// Konsoleneingabe und Speicherung der Nutzereingaben
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		int zahl1 = myScanner.nextInt(); 
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		int zahl2 = myScanner.nextInt(); 
		
		// Berechnung aller Grundrechenarten
		int ergebnisAddition = zahl1 + zahl2;
		int ergebnisSubraktion = zahl1 - zahl2;
		int ergebnisMultiplikation = zahl1 * zahl2;
		int ergebnisDivition = zahl1 / zahl2;
		int ergebnisModulo = zahl1 % zahl2;
		
		// Ausgabe der Ergebnisse
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);
		System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubraktion);
		System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation);
		System.out.print("\n\n\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivition);
		System.out.print("\n\n\nErgebnis der Division mir Rest lautet: ");
		System.out.print(zahl1 + " % " + zahl2 + " = " + ergebnisModulo);
		
		// Scanner wird geschlossen
		myScanner.close();
	}
}