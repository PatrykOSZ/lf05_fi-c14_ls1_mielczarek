// A2.2 Konsoleneingabe -> Aufgabe 2

import java.util.Scanner;

public class Aufgabe2 {
	public static void main(String[] args) {
		// Scanner Initialisation
		Scanner scan = new Scanner(System.in);
		
		// Begr��ung der Nutzers
		System.out.println("Guten Tag Lieber Nutzer\n");
		
		// Konsoleneingabe und Speicherung der Nutzereingaben
		System.out.print("Wie hei�en Sie (Vorname)? ");
		String vorname = scan.next();
		System.out.print("Wie alt sind Sie? ");
		int alter = scan.nextInt();
		
		// Ausgabe der Nutzerdaten
		System.out.println("\n************************");
		System.out.printf("%-12s %s\n", "Ihr Vorname:", vorname);
		System.out.printf("%-12s %d\n", "Ihr Alter:", alter);
		System.out.println("************************");
		
		// Scanner wird geschlossen
		scan.close();
	}
}