// 5.2 AB Anwendung Array -> Aufgabe 4

public class Aufgabe4 {
	public static void main(String[] args) {
		int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
		int[] glückszahlen = {12, 13};
		
		System.out.print("[ ");
		for(int i = 0; i < lottoZahlen.length; i++) {
			System.out.printf("%d ", lottoZahlen[i]);
			if(i == (lottoZahlen.length - 1))
				System.out.print("]\n");
		}
			
		for(int i = 0; i < lottoZahlen.length; i++) {
			for(int j = 0; j < glückszahlen.length; j++) {
				if(lottoZahlen[i] == glückszahlen[j])
					System.out.printf("Die Zahl %d ist in der Ziehung enthalten.", glückszahlen[j]);
			}
		}
	}
}