// 5.2 AB Anwendung Array -> Aufgabe 1

public class Aufgabe1 {
	public static void main(String[] args) {
		int[] zahlen = new int[10];
		
		for(int i = 0; i < 10; i++)
			zahlen[i] = i;
		
		for(int i = 0; i < 10; i++)
			System.out.print(zahlen[i] + "\n");
	}
}