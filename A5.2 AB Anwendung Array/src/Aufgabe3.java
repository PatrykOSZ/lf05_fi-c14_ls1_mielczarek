// 5.2 AB Anwendung Array -> Aufgabe 3

import java.util.Scanner;

public class Aufgabe3 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char[] eingabe = new char[5];
		
		for(int i = 0; i < eingabe.length; i++) {
			System.out.printf("Geben Sie das %d. Zeichen ein: ", i + 1);
			eingabe[i] = scan.next().charAt(0);
		}
		
		for(int i = (eingabe.length - 1); i >= 0; i--)
			System.out.printf("%c ", eingabe[i]);
		
		scan.close();
	}
}