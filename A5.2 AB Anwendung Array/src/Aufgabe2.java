// 5.2 AB Anwendung Array -> Aufgabe 2

public class Aufgabe2 {
	public static void main(String[] args) {
		int[] zahlen = new int[10];
		
		for(int i = 1; i <= 10; i++)
			zahlen[i - 1] = (i * 2) -1;
		
		for(int i = 0; i < 10; i++)
			System.out.print(zahlen[i] + " ");
	}
}