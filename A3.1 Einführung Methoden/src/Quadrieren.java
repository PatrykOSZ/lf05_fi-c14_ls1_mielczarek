// 3.1 Einf�hrung Methode -> Aufgabe 2

import java.util.Scanner;

public class Quadrieren {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		// Ausgabe des Titels
		titel();
		
		// Konsoleneingabe
		System.out.print("Geben Sie eine Zahl ein: ");
		double x = scan.nextInt();
		
		// Quadrierung der Zahl
		double ergebnis = eva(x);
		
		// Konsolenausgabe
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
		
		scan.close();
	}
	
	// Methode zur Titelausgabe
	static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}
	
	// Methode zur Quadrierung einer Zahl
	static double eva(double x) {
		return x * x;
	}
}