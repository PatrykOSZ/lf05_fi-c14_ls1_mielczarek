import java.util.Scanner;

public class MeinFahrkartenautomat6 {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		while(true) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);	
			fahrkartenAusgaben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			
			// Informationstext
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
					"vor Fahrtantritt entwerten zu lassen!\n"+
					"Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}

	public static double fahrkartenbestellungErfassen(){
		char antwort = 'j';
		int ticketTyp = 0;
		double zuZahlenderBetrag = 0.0;
		
		while(antwort == 'j') {
			do {
				System.out.print("Fahrkartenbestellvorgang:\n"
						+ "=========================\n\n"
						+ "W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n"
						+ "   Kurzstrecke Berlin [2,00 �] (1)\n"
						+ "   Einzelfahrausweis Berlin AB [3,00 �] (2)\n"
						+ "   Einzelfahrausweis Berlin BC [3,50 �] (3)\n"
						+ "   Einzelfahrausweis Berlin ABC [3,80 �] (4)\n"
						+ "   4-Fahrten-Karte Einzelfahrausweis Berlin AB [9,40 �] (5)\n"
						+ "   4-Fahrten-Karte Einzelfahrausweis Berlin BC [12,60 �] (6)\n"
						+ "   4-Fahrten-Karte Einzelfahrausweis Berlin ABC [13,80 �] (7)\n"
						+ "   Anschlusskarte Berlin A/C [1,80 �] (8)\n"
						+ "   24-Stunden-Karte [3,70 �] (9)\n\n"
						+ "Ihre Wahl: ");
				ticketTyp = scan.nextInt();
				
				if(ticketTyp < 0 || ticketTyp > 10)
					System.out.print(">>falsche Eingabe<<\n");
			}while(ticketTyp < 0 || ticketTyp > 10);
			
			double ticketPreis = ticketpreise(ticketTyp);
			
			int anzahlTickets = 0;
			do {
				System.out.print("Anzahl der Tickets: ");
				anzahlTickets = scan.nextInt();
				
				if(anzahlTickets < 1 || anzahlTickets > 10)
					System.out.println(">> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}while(anzahlTickets < 1 || anzahlTickets > 10);
			
			zuZahlenderBetrag += ticketPreis * anzahlTickets;
			
			System.out.printf("Zu zahlender Betrag (EURO): %.2f\n\n", zuZahlenderBetrag);
			System.out.print("M�chten Sie noch ein Ticket kaufen (j/n)? ");
			antwort = scan.next().charAt(0);
		}
		
		return zuZahlenderBetrag;
	}
	
	private static double ticketpreise(int ticketTyp) {
		if(ticketTyp > 0 && ticketTyp < 10) {
			if(ticketTyp == 1)
				return 2;
			else if(ticketTyp == 2)
				return 3;
			else if(ticketTyp == 3)
				return 3.5;
			else if(ticketTyp == 4)
				return 3.8;
			else if(ticketTyp == 5)
				return 9.4;
			else if(ticketTyp == 6)
				return 12.6;
			else if(ticketTyp == 7)
				return 13.8;
			else if(ticketTyp == 8)
				return 1.8;
			else if(ticketTyp == 9)
				return 3.7;
		}
		
		return 0;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = scan.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgaben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}
	
	private static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if(r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while(r�ckgabebetrag >= 2.0) {// 2 EURO-M�nzen
				muenzeAusgeben(2, "EURO");
				r�ckgabebetrag -= 2.0;
			}
			while(r�ckgabebetrag >= 1.0) { // 1 EURO-M�nzen
				muenzeAusgeben(1, "EURO");
				r�ckgabebetrag -= 1.0;
			}
			while(r�ckgabebetrag >= 0.5) { // 50 CENT-M�nzen
				muenzeAusgeben(50, "CENT");
				r�ckgabebetrag -= 0.5;
			}
			while(r�ckgabebetrag >= 0.2) { // 20 CENT-M�nzen
				muenzeAusgeben(20, "CENT");
				r�ckgabebetrag -= 0.2;
			}
			while(r�ckgabebetrag >= 0.1) { // 10 CENT-M�nzen
				muenzeAusgeben(10, "CENT");
				r�ckgabebetrag -= 0.1;
			}
			while(r�ckgabebetrag >= 0.05) { // 5 CENT-M�nzen
				muenzeAusgeben(5, "CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.printf("%7s\n", "****");
		System.out.printf("%2s", "*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%s", "*");
		System.out.printf("%2s %s", betrag, einheit);
		System.out.printf("%2s\n", "*");
		System.out.printf("%2s", "*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%7s\n", "****");
	}
}